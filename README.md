# README #

# It Crowd Arg - Test

I decided to implement the solution to this challenge using gwt + spring boot.

Stack:

* Java 8

* Spring boot 1.4.1.RELEASE (boot, web, jpa)

* GWT 2.8.0

* Gooogle Gin 2.1.2

* Postgresql

* Apache Maven 3.0.5

## Postgresql

This example uses a production postgresql db. No script will be delivered so please run the commands below to create the
itemdb database and the items table.

* sudo -u postgres psql
* CREATE DATABASE itemdb WITH OWNER = postgres ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8' CONNECTION LIMIT = -1;
* \c itemdb;
* CREATE TABLE items(id serial NOT NULL,title text,CONSTRAINT items_pkey PRIMARY KEY (id))WITH (OIDS=FALSE);ALTER TABLE items OWNER TO postgres;

## Maven

* mvn clean install
* mvn gwt:run

## Project functionality

* Add, remove,remove all, modify and pagination using spring PagingAndSortingRepository.
* Items are sorted by title using spring PageRequest.
* No duplicated items are allowed.
* A few styles from "http://www.skymira.com/" were used.
* Item text box is limited to 25 chars.
* Some confirmations pop ups are being diplayed only for UX reasons.

## Unit Tests

No unit test were included in this projects due to time limitations.

If I had had more time I would have used @SpringBootTest to make it happen.


## Logs

The project is using spring starter Logback 1.1.7. Logback file is logback.xml and output file is gwt-sprint-boot.log