package com.gwt.test.server.business.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gwt.test.common.model.Item;
import com.gwt.test.server.business.ItemBusiness;
import com.gwt.test.server.repository.ItemRepository;

@Service
@Transactional
public class ItemBusinessImpl implements ItemBusiness {

	final static Logger LOG = LoggerFactory.getLogger(ItemBusinessImpl.class);

	private final static String SORT_BY_TITLE = "title";

	@Autowired
	private ItemRepository repository;

	@Override
	public List<Item> getItems(int pageNumber) {
		LOG.info(MessageFormat.format("Getting Items with page number = {0}", pageNumber));
		pageNumber = pageNumber > 0 ? pageNumber : 1;
		PageRequest request = new PageRequest(0, pageNumber, Sort.Direction.ASC, SORT_BY_TITLE);
		return StreamSupport.stream(repository.findAll(request).spliterator(), false).collect(Collectors.toList());
	}

	@Override
	public Long saveItem(HttpServletRequest request) throws IOException {
		String itemTitle = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		LOG.info(MessageFormat.format("Saving Item with title = {0}", itemTitle));
		checkDuplicatedItem(itemTitle);
		Item item = new Item(itemTitle);
		repository.save(item);
		return item.getId();
	}

	@Override
	public void deleteItem(Long id) {
		LOG.info(MessageFormat.format("Deleting Item with id = {0}", id));
		repository.delete(id);
	}

	@Override
	public void modifyItem(HttpServletRequest request) throws IOException {
		String jsonStrings = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
		LOG.info(MessageFormat.format("Merging Item with json string = {0}", jsonStrings));
		ObjectMapper mapper = new ObjectMapper();
		Item item = mapper.readValue(jsonStrings, Item.class);
		checkDuplicatedItem(item.getTitle());
		repository.save(item);
	}

	@Override
	public void deleteAll() {
		LOG.info("Deleting all items");
		repository.deleteAll();
	}
	
	private void checkDuplicatedItem(String itemTitle){
		if(repository.findItemByTitle(itemTitle)!=null){
			throw new IllegalArgumentException();
		}
	}
}
