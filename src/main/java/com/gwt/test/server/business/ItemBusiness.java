package com.gwt.test.server.business;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.gwt.test.common.model.Item;

public interface ItemBusiness {

	List<Item> getItems(int pageNumber);

	Long saveItem(HttpServletRequest request) throws IOException;

	void deleteItem(Long id);
	
	void deleteAll();

	void modifyItem(HttpServletRequest request) throws IOException;

}
