package com.gwt.test.server.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gwt.test.common.model.Item;

@Repository
@Transactional
public interface ItemRepository extends PagingAndSortingRepository<Item, Long>{
	Item findItemByTitle(String title);
}


