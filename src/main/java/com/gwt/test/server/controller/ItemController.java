package com.gwt.test.server.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.gwt.test.common.model.Item;
import com.gwt.test.server.business.ItemBusiness;

@RestController
@RequestMapping("/rest")
public class ItemController {

	final static Logger LOG = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	private ItemBusiness itemBusiness;

	@RequestMapping(value = "/items/{pageNumber}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public List<Item> getItems(@PathVariable Integer pageNumber) {
		return itemBusiness.getItems(pageNumber);
	}

	@RequestMapping(value = "/item", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public Long saveItem(HttpServletRequest request) throws IOException {
		return itemBusiness.saveItem(request);
	}

	@RequestMapping(value = "/delete/item/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteItem(@PathVariable Long id) {
		itemBusiness.deleteItem(id);
	}

	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteAllItems() {
		itemBusiness.deleteAll();
	}

	@RequestMapping(value = "/modify/item", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public void modifyItem(HttpServletRequest request) throws IOException {
		itemBusiness.modifyItem(request);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(IOException.class)
	private void IOExceptionHandler() {
		LOG.error("IOException in ItemController java class");
	}
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(IllegalArgumentException.class)
	private void IllegalArgumentExceptionHandler() {
		LOG.error("IllegalArgumentException in ItemController java class");
	}
}
