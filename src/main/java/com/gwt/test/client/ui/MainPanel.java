package com.gwt.test.client.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwt.test.client.events.AddItemEvent;
import com.gwt.test.client.events.DeleteAllItemEvent;
import com.gwt.test.client.events.LoadEvent;
import com.gwt.test.client.model.ModelHandler;
import com.gwt.test.client.ui.component.ImageButton;
import com.gwt.test.client.ui.schedule.ReloadItemListCommand;
import com.gwt.test.common.model.Item;

/**
 * Main UI component
 */
public class MainPanel extends Composite {

	private static MainPanelUiBinder uiBinder = GWT.create(MainPanelUiBinder.class);

	interface MainPanelUiBinder extends UiBinder<Widget, MainPanel> {
	}

	@UiField
	ImageButton addButton;

	@UiField
	ImageButton clearButton;

	@UiField
	TextBox paginationBox;

	@UiField
	ImageButton loadButton;

	@UiField
	TextBox textBox;

	@UiField
	FlowPanel itemPanel;

	private Map<String, ItemWidget> itemsWidgets;

	private SimpleEventBus eventBus;

	private ModelHandler modelHandler;

	@Inject
	public MainPanel(SimpleEventBus eventBus, ModelHandler modelHandler) {
		this.eventBus = eventBus;
		initWidget(uiBinder.createAndBindUi(this));
		itemsWidgets = new HashMap<>();
		this.modelHandler = modelHandler;
		this.setPressEventToPaginationBox();
	}

	/**
	 * handle addButton clickevent event
	 * 
	 * @param e
	 */
	@UiHandler("addButton")
	void onAddButtonClick(ClickEvent e) {
		if (textBox.getText() == null || textBox.getText() == "") {
			Window.alert("Text Box cannot be null or empty");
		} else {
			String todoText = textBox.getText();
			eventBus.fireEvent(new AddItemEvent(todoText));
			textBox.setText("");
		}
	}

	@UiHandler("clearButton")
	void onClearButtonClick(ClickEvent e) {
		boolean confirm = Window.confirm("Delete all items ?");
		if (confirm) {
			eventBus.fireEvent(new DeleteAllItemEvent());
			textBox.setText("");
		}
	}

	@UiHandler("loadButton")
	void onLoadButtonClick(ClickEvent e) {
		String paginationNumber = paginationBox.getText();
		eventBus.fireEvent(new LoadEvent(paginationNumber));
	}

	public void addItemToPanel(Item t) {
		ItemWidget w = new ItemWidget(t, eventBus);
		itemPanel.add(w);
		itemsWidgets.put(t.getTitle(), w);
	}

	public void removeItemFromPanel(Item t) {
		ItemWidget todoWidget = itemsWidgets.get(t.getTitle());
		itemPanel.remove(todoWidget);
		itemsWidgets.remove(t.getTitle());
	}

	public void removeAllItem() {
		itemPanel.clear();
		itemsWidgets.clear();
	}

	public void reloadItemList() {
		removeAllItem();
		List<Item> allItems = modelHandler.getAll();
		if (allItems.size() > 0) {
			ReloadItemListCommand reloadCommand = new ReloadItemListCommand(allItems, this);
			Scheduler.get().scheduleDeferred(reloadCommand);
		}
		textBox.setText("");
	}

	private void setPressEventToPaginationBox() {
		paginationBox.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (paginationBox.isReadOnly() || !paginationBox.isEnabled()) {
					return;
				}
				Character charCode = event.getCharCode();
				int unicodeCharCode = event.getUnicodeCharCode();
				if (!(Character.isDigit(charCode) || charCode == '.' || unicodeCharCode == 0)) {
					paginationBox.cancelKey();
				}
			}
		});
	}
}
