package com.gwt.test.client.ui.schedule;

import java.util.List;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.gwt.test.client.ui.MainPanel;
import com.gwt.test.common.model.Item;

/**
 * Deferred command to do incremental UI refresh 
 * for reloading ITem in the UI
 */
public class ReloadItemListCommand implements ScheduledCommand{

	private List<Item> itemList;
	
	private MainPanel mainPanel;
	
	private int index;

	public ReloadItemListCommand(List<Item> list, MainPanel mainPanel){
		itemList = list;
		this.mainPanel = mainPanel;
		this.index = 0;
	}
	
	/**
	 * incremental add item to the panel
	 * executed after each call of Scheduler.get().scheduleDeferred(this)
	 */
	@Override
	public void execute() {
		if (index < itemList.size()){
			Item item = itemList.get(index);
			mainPanel.addItemToPanel(item);
			Scheduler.get().scheduleDeferred(this);
			index++;
		}
	}
}
