package com.gwt.test.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwt.test.client.events.DeleteItemEvent;
import com.gwt.test.client.events.ModifyItemEvent;
import com.gwt.test.client.ui.component.ImageButton;
import com.gwt.test.common.model.Item;

public class ItemWidget extends Composite {

	private static ItemWidgetUiBinder uiBinder = GWT.create(ItemWidgetUiBinder.class);

	interface ItemWidgetUiBinder extends UiBinder<Widget, ItemWidget> {
	}

	/*
	 * UI components
	 */
	@UiField
	ImageButton deleteButton;

	@UiField
	ImageButton modifyButton;

	@UiField
	TextBox textBox;

	/**
	 * current item
	 */
	private Item currentItem;

	/**
	 * event bus
	 */
	private SimpleEventBus eventBus;

	public ItemWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Inject
	public ItemWidget(Item item, SimpleEventBus eventBus) {
		this.eventBus = eventBus;
		initWidget(uiBinder.createAndBindUi(this));
		this.currentItem = item;
		textBox.setText(item.getTitle());
	}

	@UiHandler("deleteButton")
	void onAddButtonClick(ClickEvent e) {
		boolean confirm = Window.confirm("Delete item  ?");
		if (confirm) {
			eventBus.fireEvent(new DeleteItemEvent(currentItem));
		}
	}

	@UiHandler("modifyButton")
	void onModifyButtonClick(ClickEvent e) {
		if (textBox.getText() == null || textBox.getText() == "") {
			Window.alert("Text Box cannot be null or empty");
		} else {
			boolean confirm = Window.confirm("Modify item  ?");
			if (confirm) {
				String title = textBox.getText();
				currentItem.setTitle(title);
				eventBus.fireEvent(new ModifyItemEvent(currentItem));
			}
		}
	}
}
