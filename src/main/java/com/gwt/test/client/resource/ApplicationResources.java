package com.gwt.test.client.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

/**
 * Resource bundle
 * Gather images & css
 */
public interface ApplicationResources extends ClientBundle {
    public static final ApplicationResources INSTANCE = GWT.create(ApplicationResources.class);
    
    @Source("com/gwt/test/client/resource/GwtWebAppStyles.css")
    public GwtWebAppStyles style();
    
    @Source("load.png")
    @ImageOptions(repeatStyle=RepeatStyle.Both)
    ImageResource loadIcon();
    
}
