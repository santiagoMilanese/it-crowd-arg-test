package com.gwt.test.client.resource;

import com.google.gwt.resources.client.CssResource;

/**
 * Resource Bundle
 * Gather CSS Styles (see GwtWebAppStyles.css)
 *
 */
public interface GwtWebAppStyles extends CssResource {
	
	String simpleText();
	
	String itemblock();
	
	String itemPanel();
	
	String itemText();
	
	String sendButton();
	
	String paginationText();
	
}
