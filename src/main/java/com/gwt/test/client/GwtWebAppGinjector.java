package com.gwt.test.client;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.gwt.test.client.controller.WebAppController;
import com.gwt.test.client.model.ModelHandler;
import com.gwt.test.client.resource.ApplicationResources;
import com.gwt.test.client.resource.Messages;
import com.gwt.test.client.ui.MainPanel;

/**
 * Google gin injector
 * 
 * all components to inject
 */
@GinModules(GwtWebAppGinModule.class)
public interface GwtWebAppGinjector extends Ginjector {
	
	public SimpleEventBus getEventBus();
	
	public ApplicationResources getApplicationResources();
	
	public Messages getMessages();
	
	public WebAppController getWebAppController();
	
	public ModelHandler getModelHandler();
	
	public MainPanel getMainPanel(); 
}
