package com.gwt.test.client.controller;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.gwt.test.client.events.AddItemEvent;
import com.gwt.test.client.events.AddItemEventHandler;
import com.gwt.test.client.events.DeleteAllItemEvent;
import com.gwt.test.client.events.DeleteAllItmEventHandler;
import com.gwt.test.client.events.DeleteItemEvent;
import com.gwt.test.client.events.DeleteItemEventHandler;
import com.gwt.test.client.events.LoadEvent;
import com.gwt.test.client.events.LoadEventHandler;
import com.gwt.test.client.events.ModifyItemEvent;
import com.gwt.test.client.events.ModifyItemEventHandler;
import com.gwt.test.client.json.JsonHelper;
import com.gwt.test.client.model.ModelHandler;
import com.gwt.test.client.ui.MainPanel;
import com.gwt.test.common.model.Item;

/**
 * Web App Controller Manage all business events and communicate with server
 * services
 */
public class WebAppController {

	/**
	 * Event Bus
	 */
	private SimpleEventBus eventBus;

	/**
	 * Model Handler
	 */
	private ModelHandler modelHandler;

	/**
	 * main panel UI
	 */
	private MainPanel mainPanel;

	@Inject
	public WebAppController(SimpleEventBus eventBus, ModelHandler modelHandler, MainPanel mainPanel) {

		this.eventBus = eventBus;
		this.modelHandler = modelHandler;
		this.mainPanel = mainPanel;
	}

	/**
	 * Bind all events handler
	 */
	public void bindHandlers() {

		eventBus.addHandler(AddItemEvent.TYPE, new AddItemEventHandler() {

			@Override
			public void onAddItemEventHandler(AddItemEvent event) {
				addItem(event.getItemTitle());
			}
		});

		eventBus.addHandler(DeleteItemEvent.TYPE, new DeleteItemEventHandler() {

			@Override
			public void onDeleteItemEventHandler(DeleteItemEvent event) {
				deleteItem(event.getItem());
			}
		});

		eventBus.addHandler(DeleteAllItemEvent.TYPE, new DeleteAllItmEventHandler() {

			@Override
			public void onDeleteAllItemEventHandler(DeleteAllItemEvent event) {
				deleteAll();
			}
		});

		eventBus.addHandler(ModifyItemEvent.TYPE, new ModifyItemEventHandler() {

			@Override
			public void onModifyItemEventHandler(ModifyItemEvent event) {
				modifyItem(event.getItem());
			}
		});

		eventBus.addHandler(LoadEvent.TYPE, new LoadEventHandler() {

			@Override
			public void onLoadEventHandler(LoadEvent event) {
				loadItemList(event.getPageNumber());
			}

		});
	}

	/**
	 * ask server for stored Item list
	 */
	private void loadItemList(String pageNumber) {
		String pageBaseUrl = GWT.getHostPageBaseURL();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, pageBaseUrl + "/rest/items/" + pageNumber);
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable e) {
				Window.alert("error = " + e.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					String text = response.getText();
					List<Item> itemList = JsonHelper.parseDataList(text);
					modelHandler.reloadAll(itemList);
					mainPanel.reloadItemList();
				} else {
					Window.alert("error = " + response.getStatusCode());
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
			Window.alert("error = " + e.getMessage());
		}
	}

	/**
	 * delete all item from model and UI
	 */
	private void deleteAll() {
		String pageBaseUrl = GWT.getHostPageBaseURL();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, pageBaseUrl + "/rest/deleteAll");
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable e) {
				Window.alert("error = " + e.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					modelHandler.removeAll();
					mainPanel.removeAllItem();
				} else {
					Window.alert("error = " + response.getStatusCode());
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
			Window.alert("error = " + e.getMessage());
		}
	}

	/**
	 * delete a Item (ui & model) from given id
	 * 
	 * @param Item
	 */
	private void deleteItem(Item item) {
		String pageBaseUrl = GWT.getHostPageBaseURL();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.DELETE, pageBaseUrl + "/rest/delete/item/" + item.getId());
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable e) {
				Window.alert("error = " + e.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {
					modelHandler.remove(item);
					mainPanel.removeItemFromPanel(item);
				} else {
					Window.alert("error = " + response.getStatusCode());
				}
			}
		});
		try {
			rb.send();
		} catch (RequestException e) {
			e.printStackTrace();
			Window.alert("error = " + e.getMessage());
		}
	}

	/**
	 * Modify a Item (ui & model) from given id
	 * 
	 * @param Item
	 */
	private void modifyItem(Item item) {
		String pageBaseUrl = GWT.getHostPageBaseURL();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.PUT, pageBaseUrl + "/rest/modify/item");
		rb.setHeader("Content-type", "application/x-www-form-urlencoded");
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable e) {
				Window.alert("error = " + e.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (200 == response.getStatusCode()) {

				} else if (400 == response.getStatusCode()) {
					Window.alert("Not duplicated Items allowed");
					mainPanel.reloadItemList();
				} else {
					Window.alert("error = " + response.getStatusCode());
				}
			}
		});
		try {
			rb.sendRequest(item.toString(), rb.getCallback());
		} catch (Exception e) {
			e.printStackTrace();
			Window.alert("error = " + e.getMessage());
		}
	}

	/**
	 * create and add a item with given label
	 * 
	 * @param itemTitle
	 */
	private void addItem(String itemTitle) {

		String pageBaseUrl = GWT.getHostPageBaseURL();
		RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, pageBaseUrl + "/rest/item");
		rb.setHeader("Content-type", "application/x-www-form-urlencoded");
		rb.setCallback(new RequestCallback() {

			public void onError(Request request, Throwable e) {
				Window.alert("error = " + e.getMessage());
			}

			public void onResponseReceived(Request request, Response response) {
				if (201 == response.getStatusCode()) {
					Item item = new Item(itemTitle);
					item.setId(Long.parseLong(response.getText()));
					modelHandler.add(item);
					mainPanel.addItemToPanel(item);
				} else if (400 == response.getStatusCode()) {
					Window.alert("Not duplicated Items allowed");
				} else {
					Window.alert("error = " + response.getStatusCode());
				}
			}
		});
		try {
			rb.sendRequest(itemTitle, rb.getCallback());
		} catch (RequestException e) {
			e.printStackTrace();
			Window.alert("error = " + e.getMessage());
		}
	}
}
