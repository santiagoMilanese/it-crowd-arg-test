package com.gwt.test.client.events;

import com.google.gwt.event.shared.GwtEvent;
import com.gwt.test.common.model.Item;

/**
 * Modify a Item event
 */
public class ModifyItemEvent extends GwtEvent<ModifyItemEventHandler> {

	public static Type<ModifyItemEventHandler> TYPE = new Type<ModifyItemEventHandler>();

	private Item item;

	public Item getItem() {
		return item;
	}
	
	public ModifyItemEvent(Item t) {
		this.item = t;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ModifyItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ModifyItemEventHandler handler) {
		handler.onModifyItemEventHandler(this);
	}


}
