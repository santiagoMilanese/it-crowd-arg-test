package com.gwt.test.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Load event
 * @author AGI
 *
 */
public class LoadEvent extends GwtEvent<LoadEventHandler> {

	public static Type<LoadEventHandler> TYPE = new Type<LoadEventHandler>();
	
	private String pageNumber;
	
	public LoadEvent(String pageNumber ){
		this.pageNumber=pageNumber;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LoadEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoadEventHandler handler) {
		handler.onLoadEventHandler(this);
	}

	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

}
