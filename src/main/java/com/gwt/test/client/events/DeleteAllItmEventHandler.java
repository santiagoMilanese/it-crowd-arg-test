package com.gwt.test.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * DeleteItemTodoEvent event handler
 *
 */
public interface DeleteAllItmEventHandler extends EventHandler {
	void onDeleteAllItemEventHandler(DeleteAllItemEvent event);
}
