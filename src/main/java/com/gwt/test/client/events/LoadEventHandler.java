package com.gwt.test.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * LoadEvent event handler
 * @author AGI
 *
 */
public interface LoadEventHandler extends EventHandler {
	void onLoadEventHandler(LoadEvent event);

}
