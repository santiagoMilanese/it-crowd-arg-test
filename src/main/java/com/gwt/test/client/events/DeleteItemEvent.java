package com.gwt.test.client.events;

import com.google.gwt.event.shared.GwtEvent;
import com.gwt.test.common.model.Item;

public class DeleteItemEvent extends GwtEvent<DeleteItemEventHandler> {

	public static Type<DeleteItemEventHandler> TYPE = new Type<DeleteItemEventHandler>();

	private Item item;
	
	public Item getItem() {
		return item;
	}

	public DeleteItemEvent(Item t) {
		this.item = t;
	}

	@Override
	protected void dispatch(DeleteItemEventHandler handler) {
		handler.onDeleteItemEventHandler(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DeleteItemEventHandler> getAssociatedType() {
		return TYPE;
	}
}
