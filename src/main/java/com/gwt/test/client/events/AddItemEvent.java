package com.gwt.test.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Add a Item event
 */
public class AddItemEvent extends GwtEvent<AddItemEventHandler> {

	public static Type<AddItemEventHandler> TYPE = new Type<AddItemEventHandler>();

	/**
	 * item title
	 */
	private String itemTitle;

	public String getItemTitle() {
		return itemTitle;
	}

	public AddItemEvent(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	@Override
	protected void dispatch(AddItemEventHandler handler) {
		handler.onAddItemEventHandler(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AddItemEventHandler> getAssociatedType() {
		return TYPE;
	}

}
