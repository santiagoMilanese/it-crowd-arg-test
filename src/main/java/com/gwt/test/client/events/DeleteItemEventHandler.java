package com.gwt.test.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * DeleteItemEvent event handler
 * 
 */
public interface DeleteItemEventHandler extends EventHandler {
	void onDeleteItemEventHandler(DeleteItemEvent event);

}
