package com.gwt.test.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * AddItemEvent event handler
 */
public interface AddItemEventHandler extends EventHandler {
	void onAddItemEventHandler(AddItemEvent event);
}
