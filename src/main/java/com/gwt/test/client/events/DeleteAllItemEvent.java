package com.gwt.test.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Delete all item event
 */
public class DeleteAllItemEvent extends GwtEvent<DeleteAllItmEventHandler>{

	public static Type<DeleteAllItmEventHandler> TYPE = new Type<DeleteAllItmEventHandler>();

	public DeleteAllItemEvent(){
		
	}
	
	@Override
	protected void dispatch(DeleteAllItmEventHandler handler) {
		handler.onDeleteAllItemEventHandler(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DeleteAllItmEventHandler> getAssociatedType() {
		return TYPE;
	}
	
}
