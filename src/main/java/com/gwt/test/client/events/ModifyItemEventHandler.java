package com.gwt.test.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * ModifyItemEvent event handler
 */
public interface ModifyItemEventHandler extends EventHandler {
	void onModifyItemEventHandler(ModifyItemEvent event);
}
