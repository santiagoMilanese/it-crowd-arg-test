package com.gwt.test.client.model;

import java.util.ArrayList;
import java.util.List;

import com.gwt.test.common.model.Item;

public class ModelHandler {

	List<Item> itemList;

	public ModelHandler() {
		itemList = new ArrayList<>();
	}

	public void add(Item t) {
		itemList.add(t);
	}
	
	public void remove(Item t) {
		itemList.remove(t);
	}

	public void removeAll() {
		itemList.clear();
	}

	public List<Item> getAll() {
		return itemList;
	}
	
	public void reloadAll(List<Item> list) {
		itemList.clear();
		for (Item t : list) {
			add(t);
		}
	}
}
