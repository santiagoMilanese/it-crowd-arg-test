package com.gwt.test.client;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.inject.Singleton;
import com.gwt.test.client.controller.WebAppController;
import com.gwt.test.client.model.ModelHandler;
import com.gwt.test.client.resource.ApplicationResources;
import com.gwt.test.client.resource.Messages;
import com.gwt.test.client.ui.MainPanel;

/**
 * Google gin module configuration
 */
public class GwtWebAppGinModule extends AbstractGinModule{

	@Override
	protected void configure() {
		// Resources
		bind(Messages.class).in(Singleton.class);
		bind(ApplicationResources.class).in(Singleton.class);
		
		// Core
		bind(SimpleEventBus.class).in(Singleton.class);
		bind(WebAppController.class).in(Singleton.class);
		bind(ModelHandler.class).in(Singleton.class);
		
		// UI
		bind(MainPanel.class).in(Singleton.class);
	}

}
