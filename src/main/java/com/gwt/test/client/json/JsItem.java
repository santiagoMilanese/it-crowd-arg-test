package com.gwt.test.client.json;

import com.google.gwt.core.client.JavaScriptObject;

public class JsItem extends JavaScriptObject {

	protected JsItem() {
	}

	public native final String title() /*-{
		return this.title;
	}-*/;
	
	public final native String  _getId()/*-{ return ''+this.id; }-*/;
}
