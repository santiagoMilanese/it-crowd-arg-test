package com.gwt.test.client.json;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.gwt.test.common.model.Item;

public class JsonHelper {

	@SuppressWarnings("unchecked")
	public static List<Item> parseDataList(String json) {
		List<Item> itemList = new ArrayList<>();
		JSONValue jsonVal = JSONParser.parseStrict(json);
		JSONArray object = jsonVal.isArray();
		JsArray<JsItem> array = (JsArray<JsItem>) object.getJavaScriptObject();
		if (array != null) {
			for (int i = 0; i < array.length(); i++) {
				JsItem jsItem = array.get(i);
				String title = jsItem.title();
				String id = jsItem._getId();
				Item item = new Item(title);
				item.setId(Long.parseLong(id));
				itemList.add(item);
			}
		}
		return itemList;
	}
}
