package com.gwt.test.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item implements Serializable{
	
	private static final long serialVersionUID = -1933512387663623446L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	@Column(name = "title", unique = true)
	public String title;
	
	public Item(){};
	
	public Item(String title) {
		this.title = title;
	}
	
	public Item(Long id , String title) {
		this.title = title;
		this.id =id;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override 
	public String toString() { 
		StringBuilder builder = new StringBuilder();
        builder.append("{\"id\" :");
        builder.append(id);
        builder.append(", \"title\" :");
        builder.append("\""+title+"\"");
        builder.append("}");
    return builder.toString();
	}
}
